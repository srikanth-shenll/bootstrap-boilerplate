module.exports = function(grunt) {

grunt.initConfig({
  concat: {
    options: {
      separator: ';',
    },
    dist: {
      src: ['assets/js/plugins/jquery-1.11.2.min.js',
            'assets/js/plugins/bootstrap/js/bootstrap.min.js',
            'assets/js/main.js'
            ],
      dest: 'assets/dist/production.js',
    },
  },
});

grunt.loadNpmTasks('grunt-contrib-concat');
grunt.registerTask('default', ['concat']);
};
